# nasa-app-api-proxy-app

NGINX proxy app for our Nasa app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port for NGINX proxy to listen to requests on (default: `8000`)

* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)

* `APP_PORT` - Port of the APP_HOST on which the Nginx proxy will forward requests to.(default: `9000`)