server {
  listen 8000;


  location /static/js {
    alias /vol/static/static/js;
  }

  location /static {
    alias /vol/static;
  }



  location / {
    proxy_pass           http://127.0.0.1:9000;
    include              /etc/nginx/proxy_params;
    client_max_body_size 10M;
  }
}
