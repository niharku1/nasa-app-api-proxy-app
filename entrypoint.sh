set -e
envsubst '$$NGINX_HOST' < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf #The envsubst syntax is: envsubst < path of template file which is to be populated with environment variables > path of the generated configuration file.

nginx -g 'daemon off;'